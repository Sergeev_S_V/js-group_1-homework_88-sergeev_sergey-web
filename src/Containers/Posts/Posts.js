import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {PageHeader} from "react-bootstrap";
import {fetchPosts} from "../../store/actions/posts";
import AnonymousPostsList from "./AnonymousPostsList/AnonymousPostsList";
import UserPostsList from "./UserPostsList/UserPostsList";


class Posts extends Component {
  componentDidMount() {
    this.props.onFetchPosts();
  }

  renderPosts = (user, posts) => {
    if (!user) {
      return posts.map(post =>
        <AnonymousPostsList key={post._id}
                            image={post.image}
                            datetime={post.datetime}
                            author={post.author}
                            title={post.title}
                            path={`/posts/${post._id}`}/>
      )
    }

    return posts.map(post =>
      <UserPostsList key={post._id}
                     image={post.image}
                     datetime={post.datetime}
                     author={post.author}
                     title={post.title}
                     path={`/posts/${post._id}`}
                     description={post.description}/>
    );
  };

  render() {
    return (
      <Fragment>
        <PageHeader>
          Posts
        </PageHeader>
        {this.props.posts.length > 0
          ? this.renderPosts(this.props.user, this.props.posts)
          : <p>Posts is not exist</p>
        }
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    posts: state.posts.posts,
    user: state.users.user
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchPosts: () => dispatch(fetchPosts())
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Posts);