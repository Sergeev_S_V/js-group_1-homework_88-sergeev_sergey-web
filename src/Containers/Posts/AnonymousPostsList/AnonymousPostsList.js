import React from 'react';
import {Image, Panel} from "react-bootstrap";
import PropTypes from 'prop-types';

import config from '../../../config';
import textImage from '../../../assets/images/text_image.png';
import {Link} from "react-router-dom";


const AnonymousPostsList = ({image, datetime, author, title, path}) => {
  let isImage = textImage;

  if (image) {
    isImage = config.apiUrl + '/uploads/' + image;
  }

  return(
    <Panel>
      <Panel.Heading style={{display: 'flex', justifyContent: 'space-between'}}>
        <span><b>Created:</b> {datetime}</span>
        <span>by <b>{author}</b></span>
      </Panel.Heading>
      <Panel.Body>
        <Image style={{width: '150px', marginRight: '20px'}} rounded src={isImage}/>
        <Link style={{textDecoration: 'underline'}} to={path}>
           {title}
        </Link>
      </Panel.Body>
    </Panel>
  );
};

AnonymousPostsList.propTypes = {
  image: PropTypes.string,
  datetime: PropTypes.string,
  author: PropTypes.string.isRequired,
  description: PropTypes.string
};

export default AnonymousPostsList;