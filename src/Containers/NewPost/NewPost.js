import React, {Component, Fragment} from 'react';
import {PageHeader} from "react-bootstrap";
import PostForm from "../../Components/PostForm/PostForm";
import {connect} from "react-redux";
import {createPost} from "../../store/actions/posts";

class NewPost extends Component {

  componentDidMount() {
    if (!this.props.user) {
      this.props.history.push('/login');
    }
  };

  createPost = postData => {
    const token = this.props.user.token;

    this.props.onCreatePost(postData, token);
  };

  render() {
    return(
      <Fragment>
        <PageHeader>Create new post</PageHeader>
        <PostForm user={this.props.user} onSubmit={this.createPost}/>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  user: state.users.user
});

const mapDispatchToProps = dispatch => ({
  onCreatePost: (postData, token) => dispatch(createPost(postData, token))
});

export default connect(mapStateToProps, mapDispatchToProps)(NewPost);