import {CREATE_POST_SUCCESS, FETCH_POST_SUCCESS, FETCH_POSTS_SUCCESS} from "../actions/actionTypes";

const initialState = {
  posts: [],
  post: {}
};

// добавить обработку ошибок
// отправлять headers на post запрос чтобы проверить залогинен ли он или нет
// сделать валидацию на descr и images
// если незалогинен - редирект на Login когда вводишь /add_new_post


const reducer = (state = initialState, action) => {
  switch(action.type) {
    case CREATE_POST_SUCCESS:
      return {...state};
    case FETCH_POSTS_SUCCESS:
      return {...state, posts: action.posts};
    case FETCH_POST_SUCCESS:
      return {...state, post: action.post};
    default:
      return state;
  }
};

export default reducer;