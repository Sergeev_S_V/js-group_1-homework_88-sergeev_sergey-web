import React, {Component, Fragment} from 'react';
import {Panel} from "react-bootstrap";
import CommentForm from "../CommentForm/CommentForm";
import CommentItem from "../CommentItem/CommentItem";
import {fetchComments, sendComment} from "../../store/actions/comments";
import {connect} from "react-redux";

class Comments extends Component {

  componentDidMount() {
    this.props.onFetchComments(this.props.postId);
  };

  sendCommentHandler = (comment) => {
    const token = this.props.user.token;
    const postId = this.props.postId;

    this.props.onSendComment(comment, token, postId);
  };

  render() {
    return(
      <Fragment>
        {this.props.comments.length > 0
          ? this.props.comments.map(comment => {
              return <CommentItem key={comment._id}
                                  author={comment.author}
                                  datetime={comment.datetime}
                                  comment={comment.comment}/>
          })
          : <Panel.Title>The post is not have comments</Panel.Title>
        }
        {this.props.user
          ? <CommentForm onSubmit={this.sendCommentHandler}
                         postId={this.props.postId}
                         user={this.props.user}
          />
          : <Panel.Footer>
              <Panel.Title>Unauthorized user can not send comments</Panel.Title>
            </Panel.Footer>
        }
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  user: state.users.user,
  comments: state.comments.comments
});

const mapDispatchToProps = dispatch => ({
  onFetchComments: postId => dispatch(fetchComments(postId)),
  onSendComment: (comment, token, postId) => dispatch(sendComment(comment, token, postId))
});

export default connect(mapStateToProps, mapDispatchToProps)(Comments);