import React, {Component} from 'react';
import {Button, Col, Form, FormControl, FormGroup} from "react-bootstrap";

class CommentForm extends Component {
  state = {
    comment: '',
  };

  submitFormHandler = event => {
    event.preventDefault();

    const commentData = {
      comment: this.state.comment,
      postId: this.props.postId,
    };

    this.props.onSubmit(commentData);
  };

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  render() {
    return (
      <Form horizontal onSubmit={this.submitFormHandler}>
        <FormGroup controlId="commentText">
          <Col sm={10}>
            <FormControl
              componentClass="textarea"
              placeholder="Enter your comment"
              name="comment"
              value={this.state.comment}
              onChange={this.inputChangeHandler}
            />
          </Col>
        </FormGroup>

        <FormGroup>
          <Col smOffset={2} sm={10}>
            <Button bsStyle="primary" type="submit">Comment</Button>
          </Col>
        </FormGroup>
      </Form>
    );
  }
}

export default CommentForm;
