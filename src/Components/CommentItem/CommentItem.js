import React from 'react';
import {Panel} from "react-bootstrap";

const CommentItem = ({author, datetime, comment}) => {
  return(
    <Panel>
      <Panel.Heading style={{overflow: 'hidden'}}>
        <span>Author: <b>{author}</b> </span>
        <span style={{float: 'right'}}><b>created:</b> {datetime}</span>
      </Panel.Heading>
      <Panel.Body style={{wordWrap: 'break-word'}}>
        {comment}
      </Panel.Body>
    </Panel>
  );
};

export default CommentItem;