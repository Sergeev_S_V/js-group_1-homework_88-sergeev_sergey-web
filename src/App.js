import React, {Component} from 'react';
import {Route, Switch} from "react-router-dom";

import Layout from "./Containers/Layout/Layout";

import Register from "./Containers/Register/Register";
import Login from "./Containers/Login/Login";
import Posts from "./Containers/Posts/Posts";
import NewPost from "./Containers/NewPost/NewPost";
import ViewPost from "./Containers/ViewPost/ViewPost";

class App extends Component {
  render() {
    return (
      <Layout>
        <Switch>
          <Route path="/" exact component={Posts}/>
          <Route path="/posts" exact component={Posts}/>
          <Route path="/posts/:id" exact component={ViewPost}/>
          <Route path="/register" exact component={Register}/>
          <Route path="/login" exact component={Login}/>
          <Route path="/add_new_post" exact component={NewPost}/>
        </Switch>
      </Layout>
    );
  }
}

export default App;

// ViewPost - навести красоту
// вынести все стили в css
